# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  describe 'validations' do
    ## se puedo solo uno
    # it 'validate presence of title' do
    #   should validate_presence_of(:title)
    # end
    ## o se pueden varios tambien a la vez
    it 'validate presence of required fields' do
      should validate_presence_of(:title)
      should validate_presence_of(:content)
      should validate_presence_of(:user_id)
    end
  end
end
